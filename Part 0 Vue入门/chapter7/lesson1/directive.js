/* Vue.directive('test', function(el, bindings, vnode) {
	console.log(el, bindings);
}); */

Vue.directive('test', {
	bind: function(el, bindings, vnode) {
		// 初始化数据
		console.log('bind');
	},
	inserted: function(el, bindings, vnode) {
		// 获取到dom元素
		console.log('inserted');
	},
	update: function(el, bindings, vnode) {
		// 监听组件变化
		console.log('update');
	},
	componentUpdated: function(el, bindings, vnode) {
		// 监听组件变化
		console.log('componentUpdated');
	},
	unbind: function(el, bindings, vnode) {
		// 组件销毁时处理工作,如销毁定时器
		console.log('unbind');
	}
});
