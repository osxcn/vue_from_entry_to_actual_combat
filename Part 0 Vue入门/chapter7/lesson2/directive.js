Vue.directive('long-press', function(el, bindings) {
	var timer = null;
	var value = bindings.value;
	var duration = value.duration || 700;
	var fn = value.callback;
	var that = this;
	el.addEventListener('touchstart', run);
	el.addEventListener('touchend', stop);
	el.addEventListener('touchmove', stop);
	
	function run() {
		if (timer == null) {
			timer = setTimeout(function() {
				fn && fn.call(that, el, value);
				clearTimeout(timer);
			}, duration);
		}
	}
	
	function stop() {
		clearTimeout(timer);
		timer = null;
	}
});
