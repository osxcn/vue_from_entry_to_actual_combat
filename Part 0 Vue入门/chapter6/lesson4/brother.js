Vue.component('brother', {
	data() {
		return {
			text: '兄弟内容'
		};
	},
	template: '<p style="color: blue;">{{ text }}</p>',
	created() {
		var that = this;
		this.eventBus.$on('send', function(text) {
			that.text = text;
		});
	}
});