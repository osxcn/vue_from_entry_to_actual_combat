Vue.component('child', {
	template: '<p style="color: red;" @click="handleClick">{{ text }}</p>',
	data() {
		return {
			text: '子级内容'
		}
	},
	methods: {
		handleClick: function() {
			this.eventBus.$emit('send', this.text);
		}
	}
});