Vue.component('my-button', {
	props: {
		initialCount: {
			type: Number,
			default: 5,
			validator: function(value) {
				return value > 0 && value < 10;
			}
		}
	},
	data() {
		return {
			count: this.initialCount
		}
	},
	template: '<button @click="handleClick">{{count}}</button>',
	methods: {
		handleClick: function() {
			this.count++;
			this.$emit('increment', this.count);
			this.$emit('update:initialCount', this.count);
		}
	}
});