module.exports = {
  lintOnSave: false,
  devServer: {
	  port: 8090
  },
  configureWebpack: config => {
	  if (process.env.NODE_ENV === 'production') {
		  // 生产环境
	  } else {
		  // 开发环境
		  config.devtool = 'source-map'
	  }
  }
}
