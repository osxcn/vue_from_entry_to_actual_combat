import Home from './views/Home.vue';
import Hot from './views/Hot.vue';
import Recommend from './views/Recommend.vue';
import Mine from './views/Mine.vue';
import NotFound from './views/NotFound.vue';
import Side from './views/Side.vue';
import Music from './views/recommends/Music.vue';
import Sports from './views/recommends/Sports.vue';
import Game from './views/recommends/Game.vue';

export const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/hot',
        component: Hot
    },
    {
        path: '/recommend',
        components: {
            default: Recommend,
            side: Side
        },
        children: [
            {
                path: 'music',
                component: Music
            },
            {
                path: 'sports',
                component: Sports
            },
            {
                path: 'game',
                component: Game
            }
        ]
    },
    {
        path: '/mine/:id',
        component: Mine,
        beforeEnter(to, from, next) {
            const pwd = window.prompt('请输入密码');
            if (pwd === '123') {
                next();
            } else {
                next('/');
            }
        }
    },
    {
        path: '*',
        component: NotFound
    }
];