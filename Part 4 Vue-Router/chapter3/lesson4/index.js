import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import { routes } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes,
    // scrollBehavior(to, from) {   // hash模式下无savedPosition
    //     if (to.hash) {
    //         return {
    //             selector: to.hash
    //         };
    //     }
    //     return {
    //         x: 0,
    //         y: 0
    //     }
    // }
    scrollBehavior(to, from, savedPosition) {   // history模式
        if (savedPosition) {
            return savedPosition;
        } else {
            if (to.hash) {
                return {
                    selector: to.hash
                };
            }
            return {
                x: 0,
                y: 0
            }
        }
    }
});

// router.beforeEach((to, from, next) => {

// });

new Vue({
    router,
    render(h) {
        return h(App)
    }
}).$mount('#app');