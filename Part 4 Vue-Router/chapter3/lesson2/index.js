import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import { routes } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    // mode: 'history',
    routes
});

// router.beforeEach((to, from, next) => {

// });

new Vue({
    router,
    render(h) {
        return h(App)
    }
}).$mount('#app');