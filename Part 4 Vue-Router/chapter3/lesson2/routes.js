import Home from './views/Home.vue';
import Hot from './views/Hot.vue';
import Recommend from './views/Recommend.vue';
import Mine from './views/Mine.vue';
import NotFound from './views/NotFound.vue';
import Side from './views/Side.vue';
// import Music from './views/recommends/Music.vue';
// import Sports from './views/recommends/Sports.vue';
// import Game from './views/recommends/Game.vue';

export const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/hot',
        component: Hot
    },
    {
        path: '/recommend',
        components: {
            default: Recommend,
            side: Side
        },
        children: [
            {
                path: 'music',
                component: () => import(/* webpackChunkName: "music" */'./views/recommends/Music.vue')
            },
            {
                path: 'sports',
                component: () => import(/* webpackChunkName: "sports" */'./views/recommends/Sports.vue')
            },
            {
                path: 'game',
                component: () => import(/* webpackChunkName: "game" */'./views/recommends/Game.vue')
            }
        ]
    },
    {
        path: '/mine/:id',
        component: Mine,
        beforeEnter(to, from, next) {
            const pwd = window.prompt('请输入密码');
            if (pwd === '123') {
                next();
            } else {
                next('/');
            }
        }
    },
    {
        path: '*',
        component: NotFound
    }
];