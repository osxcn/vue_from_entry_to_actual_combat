import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import { routes } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    // mode: 'history',
    routes
});

function checkLogin() {
    return Promise.resolve(false);
}

router.beforeEach((to, from, next) => {
    const needLogin = to.matched.some(record => record.meta.needAuth);
    if (needLogin) {
        checkLogin().then(isLogin => {
            if (!isLogin) {
                next('/login');
            } else {
                next();
            }
        })
    } else {
        next();
    }
});

new Vue({
    router,
    render(h) {
        return h(App)
    }
}).$mount('#app');