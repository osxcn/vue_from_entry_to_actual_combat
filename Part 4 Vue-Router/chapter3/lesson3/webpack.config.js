const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpak = require('webpack');
const { resolve } = require('path');
const VuePlugins = require('vue-loader/lib/plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const dist = resolve(__dirname, '/dist');
module.exports = {
    mode: 'development',
    entry: {
        vendor: ['vue', 'vue-router'],
        app: './index.js'
    },
    output: {
        path: dist,
        filename: '[name].bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ['@babel/preset-env'],
                    plugins: [
                        '@babel/plugin-syntax-dynamic-import'
                    ]
                }
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    {
                        loader: 'css-loader'
                    }
                ]
            }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.js'
        }
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    plugins: [
        new VuePlugins(),
        new HtmlWebpackPlugin({
            template: './template.html'
        }),
        new BundleAnalyzerPlugin()
    ],
    devtool: 'source-map',
    devServer: {
        port: 8083,
        hot: true,
        open: true,
        contentBase: dist,
        historyApiFallback: true,
    }
}