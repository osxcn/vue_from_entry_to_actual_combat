const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpak = require('webpack');
const { resolve } = require('path');
const VuePlugins = require('vue-loader/lib/plugin');
const dist = resolve(__dirname, '/dist');
module.exports = {
    mode: 'development',
    entry: {
        vendor: ['vue', 'vue-router'],
        app: './index.js'
    },
    output: {
        path: dist,
        filename: '[name].bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    }
                ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        ]
    },
    resolve: {
        alias: {
            'vue': 'vue/dist/vue.js'
        }
    },
    plugins: [
        new VuePlugins(),
        new webpak.optimize.SplitChunksPlugin(),
        new HtmlWebpackPlugin({
            template: './template.html'
        })
    ],
    devtool: 'source-map',
    devServer: {
        port: 8083,
        hot: true,
        open: true,
        contentBase: dist,
        historyApiFallback: true,
    }
}