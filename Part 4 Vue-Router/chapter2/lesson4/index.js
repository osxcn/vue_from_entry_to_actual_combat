import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import { routes } from './routes';
import MyLink from './components/Link.vue'

Vue.component('my-link', MyLink);
Vue.use(VueRouter);

const router = new VueRouter({
    // mode: 'history',
    routes
});

new Vue({
    router,
    render(h) {
        return h(App)
    }
}).$mount('#app');