// routes = { path: callback }
function Router(routes) {
    this._routes = routes;

    var _this = this;

    window.addEventListener('popstate', function() {
        _this.match();
    })

    window.addEventListener('load', function() {
        _this.match();
    })
}

Router.prototype.getCurrent = function() {
    return location.pathname;
};

Router.prototype.match = function() {
    var currentPath = this.getCurrent();
    var callback = this._routes[currentPath];
    callback && callback.call(this, currentPath);
};

Router.prototype.push = function(path) {
    history.pushState(null, '', path);
    this.match();
};

Router.prototype.replace = function(path) {
    history.replaceState(null, '', path);
    this.match();
};

Router.prototype.forward = function() {
    history.forward();
};

Router.prototype.back = function() {
    history.back();
};

Router.prototype.go = function(index) {
    history.go(index);
};