var express = require('express');
var fallback = require('connect-history-api-fallback');
var app = express();

app
    .use(fallback())
    .use(express.static(__dirname))
    .listen(8080);