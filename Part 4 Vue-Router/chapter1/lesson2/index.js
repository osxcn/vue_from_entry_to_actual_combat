var $container = document.getElementById('container');
var $push = document.getElementById('push');
var $replace = document.getElementById('replace');
var $forward = document.getElementById('forward');
var $back = document.getElementById('back');

var router = new Router({
    '/foo': function() {
        $container.innerHTML = '/Foo';
    },
    '/bar': function() {
        $container.innerHTML = '/Bar';
    }
});

on($push, 'click', function() {
    router.push('/foo');
});

on($replace, 'click', function() {
    router.replace('/bar');
});

on($forward, 'click', function() {
    router.forward();
});

on($back, 'click', function() {
    router.back();
});


function on(elem, even, fn) {
    document.addEventListener ?
        elem.addEventListener(even, fn, false)
        : elem.attachEvent('on' + even, function (event) {
            var e = event || window.event;
            e.target = e.srcElement;
            fn.call(elem, e);
        });
}