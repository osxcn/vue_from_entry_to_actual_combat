// routes = { path: callback }
function Router(routes) {
    this._routes = routes;

    var _this = this;

    window.addEventListener('hashchange', function() {
        _this.match();
    })

    window.addEventListener('load', function() {
        _this.match();
    })
}

Router.prototype.getCurrent = function() {
    return location.hash.slice(1);
};

Router.prototype.match = function() {
    var currentPath = this.getCurrent();
    var callback = this._routes[currentPath];
    callback && callback.call(this, currentPath);
};

Router.prototype.push = function(path) {
    location.assign('#' + path);
};

Router.prototype.replace = function(path) {
    location.replace('#' + path);
};

Router.prototype.forward = function() {
    history.forward();
};

Router.prototype.back = function() {
    history.back();
};

Router.prototype.go = function(index) {
    history.go(index);
};